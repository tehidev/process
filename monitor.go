package process

import (
	"time"

	"github.com/elastic/go-sysinfo"
	"github.com/elastic/go-sysinfo/types"
)

// Monitor process stats
type Monitor struct {
	// CPU usage total: 0.5 = 50% of core, 2.0 = 100% of two core
	CPU float64
	// RMemory - redident memory usage in bytes
	RMemory uint64
	// VMemory - virtual memory usage in bytes
	VMemory uint64

	proc types.Process
	done chan struct{}

	lastTime  time.Time
	lastTotal time.Duration
}

// StartMonitor process - self process on pid == 0
func StartMonitor(pid int, refreshInterval time.Duration) (*Monitor, error) {
	if pid == 0 {
		pid = ID
	}
	process, err := sysinfo.Process(pid)
	if err != nil {
		return nil, err
	}
	info, err := process.Info()
	if err != nil {
		return nil, err
	}
	pm := &Monitor{
		proc:     process,
		done:     make(chan struct{}),
		lastTime: info.StartTime,
	}
	pm.Tick(time.Now())
	go pm.runTicker(refreshInterval)
	return pm, nil
}

// PID - process id
func (pm *Monitor) PID() int { return pm.proc.PID() }

// Stop refresh stats
func (pm *Monitor) Stop() { close(pm.done) }

func (pm *Monitor) runTicker(refreshInterval time.Duration) {
	ticker := time.NewTicker(refreshInterval)
	defer ticker.Stop()
	for {
		select {
		case t := <-ticker.C:
			pm.Tick(t)
		case <-pm.done:
			return
		}
	}
}

// Tick of collection stats
func (pm *Monitor) Tick(t time.Time) {
	if mem, err := pm.proc.Memory(); err == nil {
		pm.RMemory = mem.Resident
		pm.VMemory = mem.Virtual
	}
	if cpu, err := pm.proc.CPUTime(); err == nil {
		total := cpu.Total()
		since := float64(t.Sub(pm.lastTime))

		pm.CPU = float64(total-pm.lastTotal) / since

		pm.lastTime = t
		pm.lastTotal = total
	}
}
